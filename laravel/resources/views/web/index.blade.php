@extends('layouts.app')
@section('content')
<section class='row'>

    <!-- Grid column -->
    <div class='col-md-12 mb-4'>
      <!-- Card carousel-->
      <div class='card card-cascade wider reverse'>
        <!-- Card image -->
        <div class='view view-cascade overlay'>
          <div id='carouselExampleFade' class='carousel slide carousel-fade' data-ride='carousel'>
            <div class='carousel-inner'>
              <div class='carousel-item active'>
                <img class='d-block w-100 h-25' src="{{asset('cliente/img/banners/banner_1.jpg')}}" alt='First slide'>
              </div>
              <div class='carousel-item'>
                <img class='d-block w-100 h-25' src="{{asset('cliente/img/banners/banner_2.jpg')}}" alt='Second slide'>
              </div>
              <div class='carousel-item'>
                <img class='d-block w-100 h-25' src="{{asset('cliente/img/banners/banner_3.jpg')}}" alt='Third slide'>
              </div>
              <div class='carousel-item'>
                <img class='d-block w-100 h-25' src="{{asset('cliente/img/banners/banner_4.jpg')}}" alt='four slide'>
              </div>
              <div class='carousel-item'>
                <img class='d-block w-100 h-25' src="{{asset('cliente/img/banners/banner_5.jpg')}}" alt='five slide'>
              </div>
            </div>
            <a class='carousel-control-prev' href='#carouselExampleFade' role='button' data-slide='prev'>
              <span class='carousel-control-prev-icon' aria-hidden='true'></span>
              <span class='sr-only'>Previous</span>
            </a>
            <a class='carousel-control-next' href='#carouselExampleFade' role='button' data-slide='next'>
              <span class='carousel-control-next-icon' aria-hidden='true'></span>
              <span class='sr-only'>Next</span>
            </a>
          </div>
        </div>
      </div>

      <!-- nav menu -->
      <div class="container-fluid">
        <div class='row banner-card'>
          <div class='col-md-3 col-12'>
            <!-- Card Light -->
            <div class='card'>
              <!-- Card image -->
              <div class='view overlay'>
                <img class='card-img-top' src="{{asset('cliente/img/naranja.jpg')}}" alt='Card image cap'>
                <a>
                  <div class='mask rgba-white-slight'></div>
                </a>
              </div>
            </div>
  
            <div class='d-flex justify-content-center mt-2'>
              <div class='flex-column'>
                <h3><strong>Nutrición</strong> </h3>
                <!-- <h5>Control Nutricional</h5> -->
              </div>
            </div>
  
          </div>
          <div class='col-md-3 col-12'>
            <!-- Card Light -->
            <div class='card'>
  
              <!-- Card image -->
              <div class='view overlay'>
                <img class='card-img-top img-fluid' src="{{asset('cliente/img/control-niño-sano.png')}}" alt='Card image cap'>
                <a>
                  <div class='mask rgba-white-slight'></div>
                </a>
              </div>
  
            </div>
            <div class='d-flex justify-content-center mt-2'>
              <div class='flex-column'>
                <h3> <strong>Pediatría</strong> </h3>
                <!-- <h5>Sub-Title</h5> -->
              </div>
            </div>
  
          </div>
          <div class='col-md-3 col-12'>
  
            <!-- Card Light -->
            <div class='card'>
  
              <!-- Card image -->
              <div class='view overlay'>
                <img class='card-img-top img-fluid' src="{{asset('cliente/img/laboratorio.jpg')}}" alt='Card image cap'>
                <a>
                  <div class='mask rgba-white-slight'></div>
                </a>
              </div>
  
  
            </div>
            <div class='d-flex justify-content-center mt-2'>
              <div class='flex-column'>
                <h3> <strong>Laboratorio</strong> </h3>
                <!-- <h5>Sub-Title</h5> -->
              </div>
            </div>
          </div>
          <div class='col-md-3 col-12'>
            <!-- Card Light -->
            <div class='card'>
  
              <!-- Card image -->
              <div class='view overlay'>
                <img class='card-img-top  img-fluid' src="{{asset('cliente/img/healtcare/healtcare_1.png')}}" alt='Card image cap'>
                <a>
                  <div class='mask rgba-white-slight'></div>
                </a>
              </div>
            </div>
            <div class='d-flex justify-content-center mt-2'>
              <div class='flex-column'>
                <h3> <strong>Consutas</strong> </h3>
                <!-- <h5>Sub-Title</h5> -->
              </div>
            </div>
  
          </div>
  
  
  
          <!-- Card -->
  
        </div>
      </div>
     
      <!-- Grid column -->
  </section>


  <!-- segundo banner -->
  <section class='container mb-2'>
    <div class='d-flex justify-content-center'>
      <div class='text-center'>
        <h1>Especialidades</h1>
        <h6>Ofrecemos la Mejor Atención Especializada en Salud</h6>
      </div>
    </div>
  </section>

  <!-- tercer banner -->
  <section class='row'>
    <article class='container'>
      <!-- Card group -->
      <div class='card-group'>
        <!-- Card -->
        <div class='card mb-4 blue-grey lighten-4'>
          <!-- Card image -->
          <div class='view overlay'>
            <div class='d-flex flex-column'>
              <img src="{{asset('cliente/img/icons/iconToken/024-hospital-bed.png')}}" class='img-fluid w-50 h-50 m-auto pt-3' alt=''>
              <div class='d-flex flex-column text-center pt-3 m-auto'>
                <h5>Hospitalización</h5>
              </div>
            </div>
          </div>
        </div>
        <!-- Card -->

        <!-- Card -->
        <div class='card mb-4 blue-grey lighten-4'>
          <!-- Card image -->
          <div class='view overlay'>
            <div class='d-flex flex-column'>
              <img src="{{asset('cliente/img/icons/iconToken/028-dentist.png')}}" class='img-fluid w-50 h-50 m-auto pt-3' alt=''>
              <div class='d-flex flex-column text-center pt-3 m-auto'>
                <h5>Odontología</h5>
              </div>
            </div>
          </div>
        </div>
        <!-- Card -->

        <!-- Card -->
        <div class='card mb-4 blue-grey lighten-4'>
          <!-- Card image -->
          <div class='view overlay'>
            <div class='d-flex flex-column'>
              <img src="{{asset('cliente/img/icons/iconToken/032-pulse.png')}}" class='img-fluid w-50 h-50 m-auto pt-3' alt=''>
              <div class='d-flex flex-column text-center pt-3 m-auto'>
                <h5>Cardiología</h5>
              </div>
            </div>
          </div>
        </div>
        <!-- Card -->
        <!-- Card -->
        <div class='card mb-4 blue-grey lighten-4'>
          <!-- Card image -->
          <div class='view overlay'>
            <div class='d-flex flex-column'>
              <img src="{{asset('cliente/img/icons/iconToken/026-eye-drops.png')}}" class='img-fluid w-50 h-50 m-auto pt-3' alt=''>
              <div class='d-flex flex-column text-center pt-3 m-auto'>
                <h5>Oftalmología</h5>
              </div>
            </div>
          </div>
        </div>
        <!-- Card -->
        <!-- Card -->
        <div class='card mb-4 blue-grey lighten-4'>
          <!-- Card image -->
          <div class='view overlay'>
            <div class='d-flex flex-column'>
              <img src="{{asset('cliente/img/icons/iconToken/025-radioactive.png')}}" class='img-fluid w-50 h-50 m-auto pt-3' alt=''>
              <div class='d-flex flex-column text-center pt-3 m-auto'>
                <h5>Radiología</h5>
              </div>
            </div>
          </div>
        </div>
        <!-- Card -->
        <!-- Card -->
        <div class='card mb-4 blue-grey lighten-4'>
          <!-- Card image -->
          <div class='view overlay'>
            <div class='d-flex flex-column'>
              <img src="{{asset('cliente/img/icons/iconToken/023-dna.png')}}" class='img-fluid w-50 h-50 m-auto pt-3' alt=''>
              <div class='d-flex flex-column text-center pt-3 m-auto'>
                <h5>Hematología</h5>
              </div>
            </div>
          </div>
        </div>
        <!-- Card -->
        <!-- Card -->
        <div class='card mb-4 blue-grey lighten-4'>
          <!-- Card image -->
          <div class='view overlay'>
            <div class='d-flex flex-column'>
              <img src="{{asset('cliente/img/icons/iconToken/017-scissors.png')}}" class='img-fluid w-50 h-50 m-auto pt-3' alt=''>
              <div class='d-flex flex-column text-center pt-3 m-auto'>
                <h5>Traumatologia</h5>
              </div>
            </div>
          </div>
        </div>
        <!-- Card -->
      </div>
    </article>
  </section>

  <!-- quinto banner -->
  <section class='row'>
    <!-- Column -->
    <div class='col-md-4 mb-4 d-flex align-items-stretch'>

      <!--Card-->
      <div class='container-fluid '>
        <div class='row'>
          <div class='col-md-3 w-100'>
            <div class=' d-flex flex-column align-content-center align-items-center align-self-center'>
              <img src="{{asset('cliente/img/perfiles/cara1.jpg')}}" class='rounded-circle img-fluid float-right ' alt='...'>
              <h5>Nombre</h5>
              <h5>Apellido</h5>
            </div>

          </div>
          <div class='col-md-8'>
            <!--Card image-->
            <div class='view'>
              <img src="{{asset('cliente/img/comentarios/comentario1.jpg')}}" class='card-img-top img-fluid' alt=''>
              <a href='#'>
                <div class='mask rgba-white-slight'></div>
              </a>
            </div>

            <!--Card content-->
            <div class='card-body'>
              <!--Title-->
              <h4 class='card-title'>Card title</h4>
              <!--Text-->
              <p class='card-text'>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                praesentium voluptatum deleniti.</p>
            </div>
          </div>
        </div>


      </div>
      <!--/.Card-->

    </div>

    <!-- Column -->
    <div class='col-md-4 mb-4 d-flex align-items-stretch'>

      <!--Card-->
      <div class='container-fluid'>
        <div class='row'>
          <div class='col-md-3 w-100'>
            <div class=' d-flex flex-column align-content-center align-items-center align-self-center'>
              <img src="{{asset('cliente/img/perfiles/cara2.jpg')}}" class='rounded-circle img-fluid float-right ' alt='...'>
              <h5>Nombre</h5>
              <h5>Apellido</h5>
            </div>

          </div>
          <div class='col-md-8'>
            <!--Card image-->
            <div class='view'>
              <img src="{{asset('cliente/img/comentarios/comentario2.jpg')}}" class='card-img-top ' alt=''>
              <a href='#'>
                <div class='mask rgba-white-slight'></div>
              </a>
            </div>

            <!--Card content-->
            <div class='card-body'>
              <!--Title-->
              <h4 class='card-title'>Card title</h4>
              <!--Text-->
              <p class='card-text'>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                praesentium voluptatum deleniti.</p>
            </div>
          </div>
        </div>


      </div>
      <!--/.Card-->

    </div>

    <!-- Column -->
    <div class='col-md-4 mb-4 d-flex align-items-stretch'>

      <!--Card-->
      <div class='container-fluid'>
        <div class='row'>
          <div class='col-md-3 w-100'>
            <div class=' d-flex flex-column align-content-center align-items-center align-self-center'>
              <img src="{{asset('cliente/img/perfiles/cara3.jpg')}}" class='rounded-circle img-fluid float-right ' alt='...'>
              <h5>Nombre</h5>
              <h5>Apellido</h5>
            </div>

          </div>
          <div class='col-md-8'>
            <!--Card image-->
            <div class='view'>
              <img src="{{asset('cliente/img/comentarios/comentario3.jpg')}}" class='card-img-top ' alt=''>
              <a href='#'>
                <div class='mask rgba-white-slight'></div>
              </a>
            </div>

            <!--Card content-->
            <div class='card-body'>
              <!--Title-->
              <h4 class='card-title'>Card title</h4>
              <!--Text-->
              <p class='card-text'>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                praesentium voluptatum deleniti.</p>
            </div>
          </div>
        </div>


      </div>
      <!--/.Card-->

    </div>

  </section>

  <section class=' row '>
    <div class='col-md-4'>
      <!-- Card -->
      <div class='card card-image' style="background-image: url('cliente/img/card1.jpg'); background-repeat: no-repeat; background-size: cover;
        background-position: center center;">
        <!-- Content -->
        <div class='text-white text-center d-flex align-items-center rgba-black-light py-5 px-4'>
          <div>
            <h5 class='white-text'>
              <i class='fas fa-users'></i> EQUIPO
            </h5>
            <h3 class='card-title pt-2'>
              <strong>This is</strong> <strong class='blue-text'>the card title</strong>
            </h3>

            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Repellat fugiat, laboriosam, voluptatem, optio vero odio nam
              sit officia accusamus minus error nisi architecto nulla ipsum
              dignissimos. Odit sed qui, dolorum!.
            </p>
            <a class='btn btn-blue'>Ver</a>
          </div>
        </div>
      </div>
      <!-- Card -->
    </div>

    <div class='col-md-4'>
      <!-- Card -->
      <div class='card bg-card' style="background-image: url('cliente/img/card2.jpg'); background-repeat: no-repeat; background-size: cover";
        background-position: center center;'>
        <!-- Content -->
        <div class='text-white text-center d-flex align-items-center rgba-black-light py-5 px-4'>
          <div>
            <h5 class='white-text'>
              <i class='fas fa-file-signature'></i> SALUDABLE
            </h5>
            <h3 class='card-title pt-2'>
              <strong>This is</strong> <strong class='blue-text'>the card title</strong>
            </h3>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Repellat fugiat, laboriosam, voluptatem, optio vero odio nam
              sit officia accusamus minus error nisi architecto nulla ipsum
              dignissimos. Odit sed qui, dolorum!.
            </p>
            <a class='btn btn-blue'> Ver </a>
          </div>
        </div>
      </div>
      <!-- Card -->
    </div>

    <div class='col-md-4'>
      <!-- Card -->
      <div class='card ' style="background-image: url('cliente/img/card3.jpeg'); background-repeat: no-repeat; background-size: cover;
        background-position: center center;">

        <!-- Content -->
        <div class='text text-white text-center d-flex align-items-center rgba-black-light py-5 px-4'>

          <div>
            <h5 class='white-text'>
              <i class='fas fa-chart-pie'></i> MARKETING
            </h5>
            <h3 class='card-title pt-2'>
              <strong>This is</strong> <strong class='blue-text'>the card title</strong>
            </h3>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Repellat fugiat, laboriosam, voluptatem, optio vero odio nam
              sit officia accusamus minus error nisi architecto nulla ipsum
              dignissimos. Odit sed qui, dolorum!.
            </p>
            <a class='btn btn-blue'> Ver </a>
          </div>
        </div>
      </div>
      <!-- Card -->
    </div>
  </section>
@endsection