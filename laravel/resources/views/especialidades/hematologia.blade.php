<link rel='stylesheet' href="{{asset('cliente/css/estilos.css')}}">
@extends('layouts.app')
@section('content')
    <h1 class="text-center font-weight-lighter">Hematologia</h1><br>
    <div class="geko">
        <img src="{{asset('cliente/img/especialidades/hematologia.jpg')}}"  alt="hematologia">
        <article>
            <p class="text-justify">
                La hematología es la especialidad médica que se ocupa del
                estudio, diagnóstico, tratamiento y prevención de las
                enfermedades de la sangre y los órganos que participan en
                su producción, como son la médula ósea, el bazo o los
                ganglios, entre otros.
                Todas las células que forman la sangre derivan de una
                célula madre pluripotencial localizada en la médula ósea
                que mediante un proceso de diferenciación que se llama
                hematopoyesis, da lugar a diferentes tipos de células.
                Cada una de ellas posee unas característica y funciones
                específicas. Las principales células que forman la sangre
                son hematíes (o eritrocitos), leucocitos (neutrófilos,
                eosinofilos, basófilos, monocitos, linfocitos) y plaquetas.​
                La orientación de las actividades del Servicio es, como
                corresponde al medio hospitalario, fundamentalmente
                asistencial. Además, desarrolla docencia pre y postgrado,
                y actividad investigadora en la mayoría de las áreas,
                formando parte de Redes o Grupos de investigación clínica
                o básica, participando en ensayos clínicos multicéntricos
                o con proyectos de investigación de iniciativa loca
            </p>
        <article>
    </div>
@endsection
