<link rel='stylesheet' href="{{asset('cliente/css/estilos.css')}}">
@extends('layouts.app')
@section('content')
    <h1 class="text-center font-weight-lighter">Traumatologia</h1><br>
    <div class="geko">
        <img src="{{asset('cliente/img/especialidades/traumatologia.jpg')}}"  alt="traumatologia">
        <article>
            <p class="text-justify">
                La traumatología es la especialidad que incluye la
                prevención, la valoración clínica, el diagnóstico, el
                tratamiento quirúrgico y no quirúrgico y el seguimiento
                hasta el restablecimiento funcional definitivo, de los
                procesos congénitos, traumáticos, infecciosos, tumorales,
                metabólicos, degenerativos y de las deformidades y
                trastornos funcionales adquiridos del aparato locomotor
                y de sus estructuras asociadas.
                La especialidad es médico-quirúrgica, y los médicos que la
                practican se llaman traumatólogos o cirujanos ortopédicos.
                Su ámbito se extiende más allá del campo de las lesiones
                traumáticas; abarca el estudio de las enfermedades
                congénitas o adquiridas que afectan al aparato locomotor,
                especialmente de aquellas que precisan tratamiento con
                cirugía, prótesis u ortesis. Sin embargo, no todas las
                enfermedades del aparato locomotor entran dentro del
                campo de la traumatología, pues gran parte de los problemas
                de salud que afectan a las articulaciones se incluyen en el
                ámbito de la reumatología o la rehabilitación que son otras
                especialidades médicas diferentes. Algunas de las enfermedades
                de las que se ocupa la traumatología son las fracturas y
                luxaciones de los huesos, las lesiones de ligamentos, tendones
                y músculos, también los tumores óseos y numerosas afecciones de
                la columna vertebral como la hernia discal y la escoliosis.
                El servicio tiene una plantilla de 46 cirujanos ortopédicos
                además de 20 médicos residentes; se encuentra organizado en
                unidades de patología específica, como las de Miembro Superior,
                Cadera, Rodilla, Tobillo-Pie,  Fracturas de Miembro Inferior,
                Columna, Tumores y Ortopedia Infantil.
            </p>
        </div>
    </div>

@endsection
