<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" xml:lang="{{ app()->getLocale() }}" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Favicon -->
    <link rel="icon" href="{{asset('img/favicon.png')}}" type="image/png">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @guest
    @else
        <meta name="user" content="{{ Auth::user()->id }}">
    @endguest

    @include('administrador.partials.css')

    <script src="{{ asset('/js/axios.min.js')}}"></script>
    @stack('scripts-head')


    <title>{{ config('app.name') }}</title>
</head>

<body>

<main>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
                data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        @include('administrador.partials.nav')
        @include('administrador.partials.sidebar')
        {{-- @include('administrador.partials.breadcrumb') --}}
        @yield('content')

        {{-- <footer class="container-fluid">

            @include('administrador.partials.footer')
        </footer> --}}
    </div>


</main>

@include('administrador.partials.js')

@stack('scripts-footer')

</body>

</html>
