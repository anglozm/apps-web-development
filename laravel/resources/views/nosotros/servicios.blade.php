<link rel='stylesheet' href="{{asset('cliente/css/estilos.css')}}">
@extends('layouts.app')
@section('content')

<h1 class="text-center font-weight-lighter">Servicios</h1><br>
<div class="row">
	<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Alergología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Anatomopatología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Anestesiología</h3>  
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Bioanálisis</h3>     
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Cardiología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Cardiología-Hemodinamia</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Cardiología Pediátrica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Cirugía Bariátrica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Cirugía cardiovascular</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Cirugía coloproctología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Cirugía de mano</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Cirugía de torax</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Cirugía general</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Cirugía oncológica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Cirugía pediátrica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Cirugía plástica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Coloproctólogo</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Dermatología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Ecografía</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Endocrinología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Endocrinología pediátrica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Gastroenterología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Gastroenterología pediátrica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Ginecología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Gineco-obstetricia</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Hematología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Hematología pediátrica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Infectología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Inmunología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Intensivista pediátrico</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Mastología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Medicina interna</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Medicina interna-intensivista</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Medicina interna-osteoporosis</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Nefrología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Nefrología pediátrica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Neumonología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Neumonología pediátrica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Neurocirugía</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Neurología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Neurología pediátrica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Nutrición clínica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Odontología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Odontología pediátrica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Oftalmología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Oftalmología Retinología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Oncología clínica</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Otorrinolaringología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Pediatría</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Pediatría neonatología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Psiquiatría</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Radiología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Radiología Intervencionista</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Reumatología-fisiatría</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Toxicología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Traumatología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
<div class='col-sm-2'>
<div class='card'>
<div class='card-body text-justify'><h3 class='card-title text-center font-weight-lighter'>Urología</h3>
<a href='#' class='btn btn-primary'>Ver Especialista</a>
</div></div></div>
	
</div>
@endsection
