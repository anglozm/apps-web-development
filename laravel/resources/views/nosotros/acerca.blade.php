<link rel='stylesheet' href="{{asset('cliente/css/estilos.css')}}">
@extends('layouts.app')
@section('content')
<h1 class="text-center font-weight-lighter">Acerca</h1><br>
<div class="geko">
	<h2>Reseña Histórica</h2>
	<img src="{{asset('cliente/img/nosotros/acerca/clinica.jpg')}}"  alt="Clinica y Marketing">
	<article>
		<p class="text-justify">
			En la década de los años sesenta, la medicina era pública y los médicos trabajaban en centros de salud 
			públicos; tanto en el Ministerio de Sanidad y Asistencia Social como el Instituto Venezolano de los 
			Seguros Sociales. Donde se realizaban consultas externas y cirugías, ejerciéndose tanto la actividad 
			pública (gratuita) como privada. A partir del año 1.972 el Gobierno Nacional decidió que los hospitales
			públicos, (I.M.S.S e I.V.S.S) serían exclusivamente para pacientes que carecían de recursos 
			económicos, por tanto se prohibió el acceso a pacientes que tenían recursos, eliminando las 
			áreas de hospitalización privadas que existían en dichos centros de salud. Ésta decisión conllevó 
			a que gran parte de la población empezara a demandar Instituciones de Salud de Atención Privada. 
			Estimulando a que estos grupos de médicos a lo largo del territorio nacional se crearan en compañías 
			que a través de la emisión de acciones iniciaran las construcciones de innumerables centros de 
			atención médica de tipo privado. Estas instituciones fueron creciendo y desarrollándose a lo largo 
			de los años setenta, originando la aparición de clínicas como el Centro Policlínico Valencia que en
			el año 1.972 inició la construcción de su sede principal en un terreno ubicado en Naguanagua. 
			Durante cuatro (04) años se realizó la construcción y dotación de esta clínica siendo inaugurada el 
			26 de Julio de 1.976 y aperturada al público el día 01 de Agosto de 1.976. Con la puesta en servicio 
			de dos edificios modernos; una torre de hospitalización con dos pisos, cuarenta (40) 
			camas de hospitalización, cuatro (04) quirófanos de cirugía; una sala de partos con quirófano, 
			laboratorio central, servicios de radiología general, traumatología, emergencias de adultos y niños, 
			cocina y dietética, lavandería, estacionamiento y una torre de setenta y cinco (75) consultorios, 
			distribuidos en tres pisos.
		</p>
	</article>
</div>

<div class="geko">
	<h2>Clínica y Marketing</h2>
	<img src="{{asset('cliente/img/banners/banner_1.jpg')}}"  alt="Clinica y Marketing">
	<article>
		<p class="text-justify">
			Somos un Hospital privado; ubicado en Naguanagua, edo. Carabobo – Venezuela. Brindando 43 años de Salud al alcance de todos.
			En la actualidad, contamos con las mejores instalaciones donde tenemos un total de 118 camas 
			hospitalarias, para adultos y niños, unidad de cuidados intensivos para adultos, pediátricos y neonatales, once (11) 
			quirófanos, sala de partos y maternidad, unidad de oncología adultos y pediátricos, radiología general, Tomografía Axial 
			Computarizada (TAC), Resonancia Magnética (RMN), Unidad de Biología Molecular, Hemodinamia y Laboratorio clínico, entre 
			otras. En nuestras emergencias de Adultos y Niños contamos con áreas de observación y trauma shock, salas de nebulización 
			y consultorios. Todas nuestras unidades laboran las 24 horas del día, con personal altamente capacitado para
			brindarle la más alta calidad en servicio de salud a nuestros pacientes. Entre las torres “B” y “C” contamos 
			con un total de 140 consultorios médicos de las distintas especialidades médicas.
		</p>
	</article>
</div>

<div class="geko">
	<h2>Nuestros Valores</h2>
	<div class="row">
		<div class="col-sm-4">				
			<div class="card">
				<div class="card-body text-justify">
					<h3 class="card-title text-left font-weight-lighter">Calidad</h3>
					<span>
						Satisfacemos las expectativas de nuestros usuarios, 
						colaboradores y aliados con excelencia, destreza y conocimiento.
					</span>
				</div>
			</div>
		</div>

		<div class="col-sm-4">				
			<div class="card">
				<div class="card-body text-justify">
					<h3 class="card-title text-left font-weight-lighter">Humanidad</h3>
					<span>
						Entendemos las necesidades de nuestros usuarios y actuamos con empatía y calidez para apoyarle.
					</span>
				</div>
			</div>
		</div>

		<div class="col-sm-4">				
			<div class="card">
				<div class="card-body text-justify">
					<h3 class="card-title text-left font-weight-lighter">Vocación</h3>
					<span>
						Con seguridad y dedicación, queremos que nuestros usuarios vivan una experiencia de servicio satisfactoria..
					</span>
				</div>
			</div>
		</div>

		<div class="col-sm-4">				
			<div class="card">
				<div class="card-body text-justify">
					<h3 class="card-title text-left font-weight-lighter">Integridad</h3>
					<span>
						Generamos confianza con nuestras actuaciones. Somos honestos, responsables, comprometidos y con profundo sentido ético.
					</span>
				</div>
			</div>
		</div>

		<div class="col-sm-4">				
			<div class="card">
				<div class="card-body text-justify">
					<h3 class="card-title text-left font-weight-lighter">Evolución</h3>
					<span>
						Estamos comprometidos a generar conocimiento, promover la innovación, formación y desarrollo constante de nuestra gente, ofreciendo salud de vanguardia.
					</span>
				</div>
			</div>
		</div>
		<div class="col-sm-4">				
			<div class="card">
				<div class="card-body text-justify">
					<h3 class="card-title text-left font-weight-lighter">Compromiso</h3>
					<span>
						Estamos obligados y comprometidos en brindar un servicio de salud de la más alta calidad..
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection