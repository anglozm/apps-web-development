@extends('layouts.app')
@section('content')
<h1 class="text-center font-weight-lighter">Galeria</h1><br>
<div>
	<h3 class="text-center font-weight-lighter">Cirugias Relizadas</h3><br><br>
	<div class="container">
		<div class="row">

			<div class="col-sm-4">				
				<div class="card">
					<img class="card-img-top" width="50" height="200" src="{{asset('cliente/img/nosotros/galeria/trasplantedecorazon.jpeg')}}"></img>
					<div class="card-body text-justify">
						<h3 class="card-title text-left font-weight-lighter">Trasplante de corazón</h3>
						<span>
							Consiste en extirpar un corazón lesionado o enfermo y reemplazarlo por un corazón sano de un donante. 
							Los riesgos que implican son coágulos de sangre, daño a los riñones, hígado y otros órganos, ataque cardíaco 
							o accidente cerebrovascular. También aumento del riesgo de infecciones por los medicamentos y por las heridas. 
						</span>
					</div>
					<div class="card-footer">
						<h5 class="card-title font-weight-lighter">Equipo Médico</h5>	
						<ul>
							<li>Cirujano: Dr. William Dools</li>
							<li>Anestesiólogo: Dr. El Michi</li>
						</ul>					
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="card">
					<img class="card-img-top" width="200" height="200" src="{{asset('cliente/img/nosotros/galeria/aneurismacerebral.jpeg')}}"></img>
					<div class="card-body text-justify">
						<h3 class="card-title text-left font-weight-lighter">Reparación de aneurisma cerebral</h3>
						<span>
							Es un procedimiento que se practica para corregir un aneurisma (cuando existe sangrado en partes del cerebro). 
							Cuando se realiza una cirugía del cerebro, para cortar un aneurisma, puede causar problemas con el habla, 
							la memoria, la debilidad muscular, el equilibrio, la visión y otras funciones. 
							Puede producir convulsiones, un coma, un accidente cerebrovascular e inflamación cerebral. 
						</span>
					</div>
					<div class="card-footer">
						<h5 class="card-title font-weight-lighter">Equipo Médico</h5>	
						<ul>
							<li>Cirujano: Dr. Jose Canache</li>
							<li>Anestesiólogo: Dr. El Michi</li>
						</ul>					
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="card">
					<img class="card-img-top" width="200" height="200" src="{{asset('cliente/img/nosotros/galeria/higado.jpg')}}"></img>
					<div class="card-body text-justify">
						<h3 class="card-title text-left font-weight-lighter">Trauma de hígado</h3>
						<span>
							Se produce por accidentes en los que el órgano afectado es el hígado, aunque también pueden dañarse otras
 							partes del cuerpo. En esos casos, el paciente puede morir si no se separa el sangrado. 
						</span>
					</div>
					<div class="card-footer">
						<h5 class="card-title font-weight-lighter">Equipo Médico</h5>	
						<ul>
							<li>Cirujano: Dr. Lee Wu</li>
							<li>Anestesiólogo: Dra. Losly Pardton</li>
						</ul>					
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="card">
					<img class="card-img-top" width="200" height="200" src="{{asset('cliente/img/nosotros/galeria/pulmon.jpg')}}"></img>
					<div class="card-body text-justify">
						<h3 class="card-title text-left font-weight-lighter">Transplante de pulmón</h3>
						<span>
							Consiste en reemplazar uno o ambos pulmones enfermos por sanos de un donante. Para esto se hace un corte 
							en el tórax (cuando es de uno) o debajo de la mama (cuando es de ambos). Puede durar de cuatro a 12 horas. Pueden existir 
							problemas en onde se fijaron los nuevos vasos sanguíneos y vías respiratorias, además del rechazo de los nuevos órganos. 
							Hay riesgo futuro de padecer ciertos cánceres. 
						</span>
					</div>
					<div class="card-footer">
						<h5 class="card-title font-weight-lighter">Equipo Médico</h5>	
						<ul>
							<li>Cirujano: Dra. Marylin Giugni</li>
							<li>Anestesiólogo: Dr. Ricardo Arjonas</li>
						</ul>					
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="card">
					<img class="card-img-top" width="200" height="200" src="{{asset('cliente/img/nosotros/galeria/gastrica.jpg')}}"></img>
					<div class="card-body text-justify">
						<h3 class="card-title text-left font-weight-lighter">Cirugía de derivación gástrica</h3>
						<span>
							Es el procedimiento para tratar la obesidad. Usualmente se recomienda para hombres que excedan por lo 
							menos las 100 libras y en las mujeres con 80 libras de excedente en el peso. Consiste en cambiar la forma 
							como el estómago y el intestino delgado manejan el consumo de alimentos. El estómago será más pequeño. 
							Los riesgos son reacciones alérgicas a los medicamentos, coágulos de sangre, sangrados, problemas 
							respiratorios, ataque cardíaco o accidente cerebrovascular.
						</span>
					</div>
					<div class="card-footer">
						<h5 class="card-title font-weight-lighter">Equipo Médico</h5>	
						<ul>
							<li>Cirujano: Dr. Joya</li>
							<li>Anestesiólogo: Dr. Silvestre Stallone</li>
						</ul>					
					</div>
				</div>
			</div>


			
		</div>
	</div>

	<h3 class="text-center">Testimonios de Pacientes</h3><br><br>
	<div class="container">
		<div class="row">

			<div class="col-sm-4">				
				<div class="card">
					<img class="card-img-top" width="200" height="200" src="{{asset('cliente/img/nosotros/galeria/img1.jpg')}}"></img>
					<div class="card-body text-justify">
						<span>
							Bypass Gástrico
							Peso antes de la cirugía: 110 kg.
							Peso después de la cirugía: 75kg.
							Bajé: 35 kg.

							Cuando yo tenía 40 años comencé a subir de peso, hasta llegar a los 110 kg. En febrero de 2008 me diagnosticaron diabetes, y desde entonces he tomado en cuenta cualquier tratamiento que me pueda ayudar a bajar de peso y controlar mis niveles de azúcar.

							Por fin decidí ir a visitar al Dr. Joya, de quien había escuchado muy buenos comentarios. Después de una evaluación me dijo que yo sería candidato para un Bypass Gástrico.

							Tuve que aprender a comer lento y en porciones pequeñas. Tengo algunas limitaciones, pero creo que tenía mayores limitaciones antes de la cirugía.
							Estoy muy agradecido con el Dr. Joya y su equipo por darme la oportunidad de una nueva vida. 
						</span>
					</div>
					<div class="card-footer">
						<h5 class="card-title font-weight-lighter">Equipo Médico</h5>	
						<ul>
							<li>Cirujano: Dr. Federico Yarza</li>
						</ul>					
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="card">
					<img class="card-img-top" width="200" height="200" src="{{asset('cliente/img/nosotros/galeria/img2.jpg')}}"></img>
					<div class="card-body text-justify">
						<span>
							“Mi nombre es  Néstor, tengo 80 años y vivo en la ciudad de Gálvez, provincia de Santa Fe. Mi 
							problema fue el desgaste de las arterias debido  a la diabetes, lo que provocó un infarto. 
							El Dr. Marcelo Nahin me operó en la Clinica y Marketing  el día 15 de octubre del  año 2002. ¡¡¡En 
							octubre del 2017 cumpliré  15 años de operado!!!
						</span>
					</div>
					<div class="card-footer">
						<h5 class="card-title font-weight-lighter">Equipo Médico</h5>	
						<ul>
							<li>Cirujano: Dr. Marcelo Prado</li>
						</ul>					
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="card">
					<img class="card-img-top" width="200" height="200" src="{{asset('cliente/img/nosotros/galeria/img3.jpg')}}"></img>
					<div class="card-body text-justify">
						<span>
							“Mi nombre es Hilda Esther Mossuz, tengo 84 años, nací en Llambi Campbell, provincia de Santa Fe. En el año 2006 me diagnosticaron, “Lesión severa  de Tronco de la arteria coronaria izquierda, 
							lesión severa de la coronaria derecha” y además se sumó un problema en la válvula aortica “estenosis aortica severa”.
						</span>
					</div>
					<div class="card-footer">
						<h5 class="card-title font-weight-lighter">Equipo Médico</h5>	
						<ul>
							<li>Cirujano: Dr. Blue Lozada</li>
						</ul>					
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection
