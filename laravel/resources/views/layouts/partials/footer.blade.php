 <!-- Footer -->
  <footer class='page-footer font-small blue darken-4 pt-4'>
    <!-- Footer Links -->
    <div class='container-fluid text-center text-md-left'>
      <!-- Grid row -->
      <div class='row'>
        <!-- Grid column -->
        <div class='col-md-3 col-lg-3 mr-auto my-md-4 my-0 mt-4 mb-1'>
          <!-- Content -->
          <h5 class='font-weight-bold text-uppercase mb-4'>Acerca</h5>
          <p>
            Somos un Hospital; ubicado en Naguanagua, edo. Carabobo – Venezuela.
            Brindando 16 años de Salud al alcance de todos. 
            Contamos con profesionales altamente calificados, 
            quienes mediante un sistema de mejora continua 
            incrementan la calidad de los procesos y servicios.
          </p>
        </div>
        <!-- Grid column -->

        <hr class='clearfix w-100 d-md-none' />

        <!-- Grid column -->
        <div class='col-md-2 col-lg-2 mx-auto my-md-4 my-0 mt-4 mb-1'>
          <!-- Links -->
          <h5 class='font-weight-bold text-uppercase mb-4'>Nosotros</h5>

          <ul class='list-unstyled'>
            <li>
              <p>
                <a href='#!'>Quienes Somos</a>
              </p>
            </li>
            <li>
              <p>
                <a href='#!'>Junta Directiva</a>
              </p>
            </li>
            <li>
              <p>
                <a href='#!'>Servicios</a>
              </p>
            </li>
            <li>
              <p>
                <a href='#!'>Galeria</a>
              </p>
            </li>
          </ul>
        </div>
        <!-- Grid column -->

        <hr class='clearfix w-100 d-md-none' />

        <!-- Grid column -->
        <div class='col-md-4 col-lg-3 mx-auto my-md-4 my-0 mt-4 mb-1'>
          <!-- Contact details -->
          <h5 class='font-weight-bold text-uppercase mb-4'>Contacto</h5>

          <ul class='list-unstyled'>
            <li>
              <p><i class='fas fa-home mr-3'></i> Facyt Universidad de Carabobo, Venezuela</p>
            </li>
            <li>
              <p><i class='fas fa-envelope mr-3'></i> salud@uc.edu.ve</p>
            </li>
            <li>
              <p><i class='fas fa-phone mr-3'></i> +58 241-6004000</p>
            </li>
            <li>
              <p><i class='fas fa-print mr-3'></i> +58 241-8674103</p>
            </li>
          </ul>
        </div>
        <!-- Grid column -->

        <hr class='clearfix w-100 d-md-none' />

        <!-- Grid column -->
        <div class='col-md-3 col-lg-3 text-center mx-auto my-4'>
          <div class='row '>

            <figure class=' w-50 p-1'>
              <a href="{{asset('cliente/img/footer/foot1.jpg')}}">
                <img alt="picture" src="{{asset('cliente/img/footer/foot1.jpg')}}" class='img-fluid h-100 w-100' />
              </a>
            </figure>

            <figure class='w-50 p-1'>
              <a href="{{asset('cliente/img/footer/foot2.jpg')}}">
                <img alt='picture' src="{{asset('cliente/img/footer/foot2.jpg')}}" class='img-fluid h-100 w-100' />
              </a>
            </figure>
          </div>
          <div class='row'>
            <figure class='w-50 p-1'>
              <a href="{{asset('cliente/img/footer/foot3.jpg')}}">
                <img alt='picture' src="{{asset('cliente/img/footer/foot3.jpg')}}" class='img-fluid h-100 w-100' />
              </a>
            </figure>

            <figure class='w-50 p-1'>
              <a href="{{asset('cliente/img/footer/foot4.png')}}">
                <img alt='picture' src='{{asset('cliente/img/footer/foot4.png')}}' class='img-fluid h-100 w-100' />
              </a>
            </figure>
          </div>
        </div>
      </div>
    </div>
    <!-- Grid column -->
    </div>
    <!-- Grid row -->
    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class='footer-copyright text-center py-3'>
      © 2020 Copyright:
      <a href='https://mdbootstrap.com/education/bootstrap/'>
        Desarrolladores Facyt</a>
    </div>
    <!-- Copyright -->
  </footer>