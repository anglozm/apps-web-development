<header class="topbar" data-navbarbg="skin6">
    <nav class="navbar top-navbar navbar-expand-md">
        <div class="navbar-header" data-logobg="skin6">
            <!-- This is for the sidebar toggle which is visible on mobile only -->
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                    class="ti-menu ti-close"></i></a>
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <div class="navbar-brand m-2">
                <!-- Logo icon -->
                <a href="index.html">
                    <b class="logo-icon">
                        <!-- Dark Logo icon -->
                        {{-- <img src="{{asset('cliente/img/logo.svg')}}" alt="homepage" class="dark-logo w-25" /> --}}
                        <!-- Light Logo icon -->
                        <img src="{{asset('cliente/img/logo.svg')}}" alt="homepage" class="light-logo w-md-25 w-100" />
                    </b>
               
                </a>
            </div>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Toggle which is visible on mobile only -->
            <!-- ============================================================== -->
            <a class="topbartoggler d-block d-md-none text-dark" href="javascript:void(0)"
                data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                    class="ti-more"></i></a>
                   
        </div>
        <div class="navbar-collapse collapse" id="navbarSupportedContent">
        <ul class="navbar-nav float-right">
            <!-- ============================================================== -->
            <!-- Search -->
            <!-- ============================================================== -->
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0)"> Ingresar
                    {{-- <form> --}}
                        {{-- <div class="customize-input">
                            <input class="form-control custom-shadow custom-radius border-0 bg-white"
                                type="search" placeholder="Search" aria-label="Search">
                            <i class="form-control-icon" data-feather="search"></i>
                        </div> --}}
                    {{-- </form> --}}
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="javascript:void(0)"> Registrar
                    {{-- <form> --}}
                        {{-- <div class="customize-input">
                            <input class="form-control custom-shadow custom-radius border-0 bg-white"
                                type="search" placeholder="Search" aria-label="Search">
                            <i class="form-control-icon" data-feather="search"></i>
                        </div> --}}
                    {{-- </form> --}}
                </a>
            </li>
        </ul>
        </div>
        </div>
    </nav>
</header>