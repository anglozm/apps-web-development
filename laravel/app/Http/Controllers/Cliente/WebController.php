<?php

namespace App\Http\Controllers\Cliente;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index(){
        return view('web.index');
    }

    public function acerca(){
        return view('nosotros.acerca');
    }

    public function servicios(){
        return view('nosotros.servicios');
    }

    public function galeria(){
        return view('nosotros.galeria');
    }

    public function traumatologia(){
        return view('especialidades.traumatologia');
    }

    public function hematologia(){
        return view('especialidades.hematologia');
    }
}
