<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['middleware' => ['passwordexp']], function () {
        Route::get('/admin', function () {
            return "hola";
            // return view('administrador.home');
        });
    // Route::get('/admin', 'Admin\AdminController@index')->name('admin.home');
});


/* Roles */
// Route::group(
//     [
//         'prefix' => 'admin',
//         'namespace' => 'Usuarios',
//     ],
//     function () {
//         /*RUTAS DE CRUD PARA  Categorias*/
//         Route::resource('usuarios', 'UsuariosController')->middleware('passwordexp');
//         Route::get('/cambiarContrasena/{usuario}/edit', 'UsuariosController@cambiarContrasena')->name('usuario.cambiarContrasena');
//         Route::post('updatecambiarContrasena/{usuario}', 'UsuariosController@updateContrasena')->name('usuario.updateContrasena');
//         Route::post('/usuariosDesactivar', 'UsuariosController@desactivar')->name('usuario.desactivar')->middleware('passwordexp');
//         Route::post('/usuariosRestablecer', 'UsuariosController@restablecerContrasena')->name('usuario.restablecer')->middleware('passwordexp');
//     }
// );
