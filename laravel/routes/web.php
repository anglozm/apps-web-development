<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/Auth::routes(['verify' => true]);

Route::get('/','Cliente\WebController@index')->name('cliente.index') ;
Route::get('/acerca','Cliente\WebController@acerca')->name('cliente.acerca') ;
Route::get('/servicios','Cliente\WebController@servicios')->name('cliente.servicios') ;
Route::get('/galeria','Cliente\WebController@galeria')->name('cliente.galeria') ;
Route::get('/traumatologia','Cliente\WebController@traumatologia')->name('cliente.traumatologia');
Route::get('/hematologia','Cliente\WebController@hematologia')->name('cliente.hematologia');
Route::group(
        [
            'prefix' => 'admin',
            'namespace' => 'Admin',
        ],
        function () {

           Route::get('/login', 'AdminController@login')->name('admin.login');


           /*RUTAS DE CRUD PARA  ADMINISTRADOR*/
            Route::get('/',function(){
                return view('administrador.home');
            }) ;

        });


